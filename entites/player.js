class Player {

    constructor(phr, x, y) {
        this.pharser = phr;
        this.sprite = this.pharser.physics.add.sprite(x, y, 'player');
        this.sprite.setGravityY(500);
    }

    setWorld(world) {
        this.world = world;
    }

    keyboard_listener() {
        //KEYUP EVENTS
        this.pharser.input.keyboard.on('keyup-A', () => {
            this.sprite.setVelocity(0, this.sprite.body.velocity.y);
        });

        this.pharser.input.keyboard.on('keyup-D', () => {
            this.sprite.setVelocity(0, this.sprite.body.velocity.y);
        });
    }

    mouse_listener() {
        //Cursor event (For highlighting blocks)
        this.pharser.input.on('pointermove', (pointer) => {
            const gridX = Math.floor(pointer.worldX / BLOCKSIZE);
            const gridY = Math.round(pointer.worldY / BLOCKSIZE);

            if (cursor == null) {
                cursor = this.pharser.add.rectangle((gridX * BLOCKSIZE), (gridY * BLOCKSIZE), BLOCKSIZE, BLOCKSIZE, getColor('ffffff'), 99);
                return true;
            }

            cursor.setPosition((gridX * BLOCKSIZE) + BLOCKSIZE / 2, (gridY * BLOCKSIZE));
        });

        //Cursor event for breaking blocks
        this.pharser.input.on('pointerdown', (pointer, currentlyOver) => {
            const block = currentlyOver[0];

            if (block == undefined){ return; }

            this.world.removeBlock(block);

            //console.log(`Clicked at: (${gridX}, ${gridY})`);
        });
    }

    update() {
        if (AKey.isDown) {
            this.sprite.setVelocity(-600, this.sprite.body.velocity.y);
        }

        if (DKey.isDown) {
            this.sprite.setVelocity(600, this.sprite.body.velocity.y);
        }

        if (SpcKey.isDown && this.sprite.body.velocity.y == 0) {
            this.sprite.setVelocity(this.sprite.body.velocity.x, -450);
        }
    }
}